package one;
import java.util.*;
public class Arraycart {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] cart = {"round-neck", "collared", "hooded", "round-neck", "round-neck"};
		int d = 0;
		int tc = 0;
		int f = 0;
		
		for (String type : cart) {
			  if (type.equals("round-neck")) {
			    tc += 200;
			  } 
			  else if (type.equals("collared")) {
			    tc += 250;
			  } 
			  else if (type.equals("hooded")) {
			    tc += 300;
			  }
			}

			if (cart.length < 5) {
			  d = 0;
			} 
			else if (cart.length >= 5 && cart.length <= 10) {
			  d = 10;
			} 
			else if (cart.length > 10) {
			  d = 20;
			}

			f = tc - ( (d / 100));
			System.out.println("The final amount for the T-Shirts is: Rs." + f);
		
	}
	}